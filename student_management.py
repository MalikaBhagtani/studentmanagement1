from openerp import fields, models, api
from openerp.exceptions import  ValidationError


class student_student(models.Model):
    _name = 'student.student'
    _description = 'Student Management'
    _order='sequence'

    @api.model
    def create(self, vals):
        seq = self._get_student_sequence()
        vals.update({'roll_no' : seq})
        return super(student_student, self).create(vals)
    
#     @api.model
#     def _get_student_sequence(self):
#         """
#         This method is used to generate the value for default value of Employee ID from sequence
#         -----------------------------------------------------------------------------------------
#         """
#         seq_obj = self.env['ir.sequence']
#         seq = seq_obj.get('student.student')
#         print "SEQ",seq
#         return seq
    
    
    @api.multi
    def demo_create_department(self):
        #create mthd
            vals = {"name": 'DrishtiGroup1'}
            dept_obj = self.env ['department.department' ].create(vals)
            print dept_obj
            print self.env
            return True
        #search mthd
            dept_obj=self.env['department.department']
            dept_rec_obj=dept_obj.search(['|'('dept_name','=','DrishtiTech')])
            dept_rec_obj.write(vals)
            res= dept_rec_obj
            return res
            return True
            print "--dept-obj--" ,dept_obj
            print "--dept_rec_obj--" ,dept_rec_obj
            print "--res--" ,res
        #browse mthd
            department_id=1
            dept_obj=self.env['department.department'].browse(department_id)
            dept_obj.write({'name':'Geog','department_id':'G'})
        
    @api.multi
    def unlink(self):
            res= super(student_student,self).unlink()
            print res
    
    
      
    @api.multi
    @api.depends('marksheet_lines.marks_obtained')
    def cal_total_marks(self):
            value=0
            for line in self.marksheet_lines:
                    value+=line.marks_obtained
                    self.marks_obtained=value
                    
#     @api.model
#     def fields_view_get(self,view_id=None,view_type='form',context=None,toolbar=False,submenu=False):  
#         res=super(student_student,self).fields_view_get(view_id=view_id, view_type=view_type, 
#                                                 context=context, toolbar=toolbar, submenu=submenu)
#         for field in res['fields']:
#             if field == 'property_department_id':
#                 dept_objs=self.env['department.department'].search([('department_code','in'['DG','Geo'])])
#                 dept_ids = dept_objs.ids
#                 res['fields'][field]['domain'] = [('id','in',dept_ids)]
#                 
#         print "RES fields_view_get>>>>>>>>>>"
#         print(res)
#         return  res
                    
#     @api.onchange('stream') 
#     def onchange_stream(self):
#             if self.stream =='Commerce':
#                 self.roll_no ='CM'
#             elif self.stream =='Science':
#                 self.roll_no ='SC'
#             elif self.stream =='Arts':
#                 self.roll_no ='AR'
#             else:
#                 self.roll_no =''
              
    @api.model
    def create_mthd(self,vals):
            print vals
            res=super(student_student,self).create(vals)
            print res
            return res
        
    @api.multi
    def write(self,vals):
        print "---userid---",self._uid
        print '---record id--',self.id
        print '---record ids---',self.ids
        print'---context---',self._context
#            print '---args---',self._args
        print "----write method executed------"
        res=super(student_student,self).write(vals)
        print vals
        print res
        return res
    
    sequence=fields.Integer('Sequence')
    roll_no=fields.Char('Roll No')
    name=fields.Char(string ='Name')
    address=fields.Char(string = 'Address')
    stream=fields.Selection([('Science','science'),('Commerce','commerce'),('Arts','arts')], string='Stream', default='Science')
    gender=fields.Selection([('Female','female'),('Male','male')], string='gender',default="Female")
    location=fields.Char(string = 'Location')
    dob=fields.Date(string = 'DOB')
    date_today=fields.Datetime(string='Todays Date')
    contact=fields.Char(string = 'Contact', size=10)
    property_department_id=fields.Many2one('department.department',string='department',company_dependent=True)
    marksheet_lines=fields.One2many('marksheet.marksheet','student_id',string='marksheet')
    remarks=fields.Text("remark")
    subject_ids=fields.Many2many('subject.subject','student_subject_rel','student_id','subject_id')
    image_file=fields.Binary("Image")
    marks_obtained=fields.Integer('Total Marks Obtained',compute=cal_total_marks)
    is_bio=fields.Boolean('is a bio student',default='False')
    state = fields.Selection(selection=[('applied','Applied'),
                                        ('interviewed','Interviewed'),
                                        ('selected','Selected'),
                                        ('rejected','Rejected'),
                                        ('joined','Joined'),
                                        ('terminated','Terminated'),
                                        ('left','Left')], string='State', default='applied')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)       
  
    @api.multi
    def interview(self):
        for emp in self:
            emp.write({'state':'interviewed'})
        return True
    
        
    
    @api.multi
    def select(self):
        
        for emp in self:
            emp.write({'state':'selected'})
        return True
    
    @api.multi
    def reject(self):
        
        for emp in self:
            emp.write({'state':'rejected'})
        return True
    
    @api.multi
    def join(self):
       
        for emp in self:
            emp.write({'state':'joined'})
        return True
    
    @api.multi
    def terminate(self):
        
        for emp in self:
            emp.write({'state':'terminated'})
        return True
    
    @api.multi
    def leave(self):
       
        for emp in self:
            emp.write({'state':'left'})
        return True
    
    def apply(self):
       
        for emp in self:
            emp.write({'state':'applied'})
        return True
   
              
    _sql_constraints=[
                          ('roll_number_unique','unique(roll_no)','Roll No must be Unique')
                         ]
#     @api.multi
#     def confirm_selection(self):
#             for student in self:
#                 student.state='selected'
#             if not student.name:
#                 raise Warning('Name cannot be empty')
#                 raise Warning('Name cannot be displayed')
#             else:
#                 student.state='selected'
#             print self._context
#             print "state",self._context.get('state',False)
            
    @api.multi
    def render_departments(self):
        model_obj = self.env['ir.model.data']
        tree = model_obj.get_object_reference('studnt_management', 'department_department_tree_viewl')
        form = model_obj.get_object_reference('studnt_management', 'department_department_form_view')
        print tree
        print form 
        tree_id = tree[1]
        form_id = form[1]
        return {'type': 'ir.actions.act_window',
                'res_model': 'department.department',
                'res_id': 2,
                'view_mode': 'form,tree',
                'view_type': 'form',
                'views': [(form_id, 'form'),(tree_id, 'tree')],
                'target': 'current'
#                 'domain': []
#                 'context': 
                }
        
    @api.constrains('remarks')
    def check_remark(self):
            for student_object in self:
                remark=student_object.remarks
            if not remark or len(remark)<20:
                raise ValidationError('Remarks to be at least 20 characters')
    @api.model
    def default_get(self,fields_list):
        res=super(student_student,self).default_get(fields_list)
        print "old res",res
        res.update ({'gender':'Female'})
        print "new res",res
        return res
          
           
            
class subject_subject(models.Model):
    _name='subject.subject'
    
    name=fields.Char(string ='Name')
    code=fields.Char(string ='Code')

class semester_semester(models.Model):
    _name='semester.semester'
    _description='semester details'
    
    id=fields.Char(string='id')
    name=fields.Char(string='name')
    
class marksheet_marksheet(models.Model):
    _name='marksheet.marksheet'
    _description='Marksheet Details'
    
    @api.multi
    @api.depends('marks_obtained','total')
    def cal_percentage(self):
        for record in self:
            total=float(record.total)
            marks=float(record.marks_obtained)
            try:
                percentage=(marks/total) * 100
            except ZeroDivisionError:
                record.percentage=0.0
            else:
                record.percentage=percentage
        
    sem=fields.Many2one('semester.semester',string='sem')
    marks_obtained=fields.Integer('Marks Obtained')
    total=fields.Integer('Total',default=700)
    percentage=fields.Float("percentage",compute=cal_percentage)
    student_id=fields.Many2one('student.student',string='student')
        

class department_department(models.Model):
    _name='department.department'
    _description='Department Details'
#     _rec_name='department_name'
    
                                                                                                                                                                                
    name=fields.Char(string ='Name')  
    
    department_code=fields.Char(string='Code')
    department_phone=fields.Integer(string='Phone_No')
    department_fax=fields.Integer(string='Fax')
    
#     @api.multi
#     def name_get(self):
#         res=[]
#         for department in self:
#             dept_str=''
#         if department.department_code:
#             dept_str+='['+department.department_code+']'
#         else:
#             pass
#         if department.name:
#              dept_str+=department.name
#              res.append((department.id,dept_str))
#                        
#     @api.model
#     def name_search(self,name='',args=None,operator='ilike',limit=True):      
#         args+=['|',('code',operator,name)]  
#         deps=self.search(args,limit=limit)  
#         return deps.name_get()   
#     
            
class faculty_faculty(models.Model):
    _name='faculty.faculty'
    _description='Faculty Details'
    
          
    fno=fields.Char(string='id')
    name=fields.Char(string='name')
    contact=fields.Integer(string='contact')


    
    

    