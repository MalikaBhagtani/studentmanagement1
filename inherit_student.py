from openerp import fields, models

class student_student(models.Model):
    _inherit='student.student'
    
    bloodgroup=fields.Char(string='Bloodgroup')
    email=fields.Char(string='Email')
    
class student_alumini(models.Model):
    _name='student.alumini'
    _inherits={'student.student':'student_id'}
    
    student_id=fields.Many2one('student.student','student',required =True,ondelete='cascade',selected=True)
    occupation=fields.Char('occupation')
    married=fields.Boolean('Married')
    age=fields.Integer('age')
    