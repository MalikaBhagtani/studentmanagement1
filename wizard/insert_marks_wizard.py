from openerp import fields,api,models

class insert_marks_wizard(models.TransientModel):
    _name = 'insert.marks.wizard'
    
    sem=fields.Many2one('semester.semester',string='sem')
    marks_obtained=fields.Integer('Marks Obtained')
    total=fields.Integer('Total',default=700)
    
    @api.multi
    def update_marks(self):
        print ">>>>>>>>>>>>>>>>>>>>>",self._context
        sem = self.sem
        marks_obtained = self.marks_obtained
        total = self.total
        ctx = self._context
        student_id = ctx.get('active_id',False)
        if student_id:
            stud_obj = self.env['student.student'].browse(student_id)
            vals= {'marksheet_line': [(0,0, {'sem': sem, 'marks_obtained': marks_obtained, 'total': total})] }
            stud_obj.write(vals)
            
        return True