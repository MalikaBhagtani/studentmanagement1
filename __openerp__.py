{
"name":"student.management",
"author":"Fresher",
"version" : "1.0",
"installable":True,
"auto_install":False,
"depends":['base'],
"data":[
        
        "student_management.xml",
        'data/student_data.xml',
        'inherit_student.xml',
        'student_workflow.xml',
        'data/student_sequence.xml',
        'wizard/insert_marks_wizard.xml',
        'security/student_security.xml',
        'security/ir.model.access.csv',]
}
